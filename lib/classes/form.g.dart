// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Formulario on _FormularioBase, Store {
  Computed<bool> _$isValidComputed;

  @override
  bool get isValid => (_$isValidComputed ??=
          Computed<bool>(() => super.isValid, name: '_FormularioBase.isValid'))
      .value;

  final _$namesAtom = Atom(name: '_FormularioBase.names');

  @override
  ObservableList<String> get names {
    _$namesAtom.reportRead();
    return super.names;
  }

  @override
  set names(ObservableList<String> value) {
    _$namesAtom.reportWrite(value, super.names, () {
      super.names = value;
    });
  }

  final _$agesAtom = Atom(name: '_FormularioBase.ages');

  @override
  ObservableList<String> get ages {
    _$agesAtom.reportRead();
    return super.ages;
  }

  @override
  set ages(ObservableList<String> value) {
    _$agesAtom.reportWrite(value, super.ages, () {
      super.ages = value;
    });
  }

  final _$_FormularioBaseActionController =
      ActionController(name: '_FormularioBase');

  @override
  dynamic addName(String newName) {
    final _$actionInfo = _$_FormularioBaseActionController.startAction(
        name: '_FormularioBase.addName');
    try {
      return super.addName(newName);
    } finally {
      _$_FormularioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic removeName(int index) {
    final _$actionInfo = _$_FormularioBaseActionController.startAction(
        name: '_FormularioBase.removeName');
    try {
      return super.removeName(index);
    } finally {
      _$_FormularioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic validateNames() {
    final _$actionInfo = _$_FormularioBaseActionController.startAction(
        name: '_FormularioBase.validateNames');
    try {
      return super.validateNames();
    } finally {
      _$_FormularioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addAge(String newAge) {
    final _$actionInfo = _$_FormularioBaseActionController.startAction(
        name: '_FormularioBase.addAge');
    try {
      return super.addAge(newAge);
    } finally {
      _$_FormularioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic removeAge(int index) {
    final _$actionInfo = _$_FormularioBaseActionController.startAction(
        name: '_FormularioBase.removeAge');
    try {
      return super.removeAge(index);
    } finally {
      _$_FormularioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic validateAges() {
    final _$actionInfo = _$_FormularioBaseActionController.startAction(
        name: '_FormularioBase.validateAges');
    try {
      return super.validateAges();
    } finally {
      _$_FormularioBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
names: ${names},
ages: ${ages},
isValid: ${isValid}
    ''';
  }
}
