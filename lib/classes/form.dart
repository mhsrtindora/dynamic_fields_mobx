import 'package:mobx/mobx.dart';
part 'form.g.dart';

class Formulario = _FormularioBase with _$Formulario;

abstract class _FormularioBase with Store {
  @observable
  var names = ObservableList<String>();
  @action
  changeName(String value, int index) => names[index] = value;
  @action
  addName(String newName) => names.add(newName);
  @action
  removeName(int index) => names.removeAt(index);
  @action
  validateNames() {
    for (var name in names) {
      if (name.isEmpty) {
        return false;
      } else {
        return true;
      }
    }
  }

  @observable
  var ages = ObservableList<String>();
  @action
  changeAge(String value, int index) => ages[index] = value;
  @action
  addAge(String newAge) => ages.add(newAge);
  @action
  removeAge(int index) => ages.removeAt(index);
  @action
  validateAges() {
    for (var age in ages) {
      if (age.isEmpty) {
        return false;
      } else {
        return true;
      }
    }
  }

  @computed
  bool get isValid {
    return validateNames() == true && validateAges() == true;
  }

  removeFields(int index) {
    removeName(index);
    removeAge(index);
  }

  newFields(name, age) {
    addName(name);
    addAge(age);
  }

  submit() {
    print("Funfou");
  }
}
