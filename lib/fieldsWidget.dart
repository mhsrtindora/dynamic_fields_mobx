import 'package:dynamic_fields_traduzido/classes/form.dart';
import 'package:flutter/material.dart';

class FieldsWidget extends StatelessWidget {
  const FieldsWidget({
    this.index,
    this.nameController,
    this.ageController,
  });

  final int index;
  final TextEditingController nameController;
  final TextEditingController ageController;

  @override
  Widget build(BuildContext context) {
    Formulario form = Formulario();
    return Row(
      children: <Widget>[
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Text('${index + 1}:')),
        Expanded(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: TextField(
                  controller: nameController,
                  style: const TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    labelText: 'Name:',
                    hintText: 'Insert a name...',
                    errorText: "snapshot.error",
                  ),
                  onChanged: form.changeName(nameController.text, index),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: TextField(
                  controller: ageController,
                  style: const TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    labelText: 'Age:',
                    hintText: 'Insert the age (1 - 999).',
                    errorText: "snapshot.error",
                  ),
                  keyboardType: TextInputType.number,
                  onChanged: form.changeAge(ageController.text, index),
                ),
              ),
              const SizedBox(
                height: 22.0,
              ),
            ],
          ),
        ),
        IconButton(
          icon: const Icon(Icons.delete),
          color: Colors.red,
          onPressed: () => form.removeFields(index),
        ),
      ],
    );
  }
}
