import 'package:dynamic_fields_traduzido/dynamicFields.dart';
import 'package:flutter/material.dart';

class DynamicFieldsPage extends StatefulWidget {
  @override
  _DynamicFieldsPageState createState() => _DynamicFieldsPageState();
}

class _DynamicFieldsPageState extends State<DynamicFieldsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Dynamic fields validation'),
        ),
        body: DynamicFieldsWidget(),
      ),
    );
  }
}
