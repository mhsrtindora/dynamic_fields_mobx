import 'package:dynamic_fields_traduzido/classes/form.dart';
import 'package:dynamic_fields_traduzido/fieldsWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class DynamicFieldsWidget extends StatelessWidget {
  final nameFieldsController = List<TextEditingController>();
  final ageFieldsController = List<TextEditingController>();

  @override
  Widget build(BuildContext context) {
    Formulario form = Formulario();

    List<Widget> _buildFields(int length) {
      nameFieldsController.clear();
      ageFieldsController.clear();

      for (int i = 0; i < length; i++) {
        final name = form.names[i];
        final age = form.ages[i];

        nameFieldsController.add(TextEditingController(text: name));
        ageFieldsController.add(TextEditingController(text: age));
      }

      return List<Widget>.generate(
        length,
        (i) => FieldsWidget(
          index: i,
          nameController: nameFieldsController[i],
          ageController: ageFieldsController[i],
        ),
      );
    }

    return ListView(
      children: <Widget>[
        Container(
          height: 16.0,
        ),
        Column(children: _buildFields(form.names.length)),
        Container(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            RaisedButton(
              color: Colors.green,
              child: const Text(
                'Add more fields',
              ), //style: buttonText),
              onPressed: form.newFields(
                  nameFieldsController[0].text, ageFieldsController[0].text),
            ),
            Observer(builder: (_) {
              return RaisedButton(
                color: Colors.blue,
                child: const Text('Submit'), //style: buttonText),
                onPressed: form.isValid ? form.submit : null,
              );
            }),
          ],
        ),
      ],
    );
  }
}
